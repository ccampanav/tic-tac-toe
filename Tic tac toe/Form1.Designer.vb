﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Home
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Home))
        Me.LabelTitle = New System.Windows.Forms.Label()
        Me.LabelJ1 = New System.Windows.Forms.Label()
        Me.LabelJ2 = New System.Windows.Forms.Label()
        Me.LabelLoad = New System.Windows.Forms.Label()
        Me.LabelM1 = New System.Windows.Forms.Label()
        Me.LabelM2 = New System.Windows.Forms.Label()
        Me.LabelNum = New System.Windows.Forms.Label()
        Me.J1 = New System.Windows.Forms.Label()
        Me.J2 = New System.Windows.Forms.Label()
        Me.LabelStart = New System.Windows.Forms.Label()
        Me.LabelEstado = New System.Windows.Forms.Label()
        Me.LabelName1 = New System.Windows.Forms.Label()
        Me.LabelName2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.LabelPartida = New System.Windows.Forms.Label()
        Me.PictureSonido = New System.Windows.Forms.PictureBox()
        Me.Picture7 = New System.Windows.Forms.PictureBox()
        Me.Picture8 = New System.Windows.Forms.PictureBox()
        Me.Picture9 = New System.Windows.Forms.PictureBox()
        Me.Picture3 = New System.Windows.Forms.PictureBox()
        Me.Picture4 = New System.Windows.Forms.PictureBox()
        Me.Picture5 = New System.Windows.Forms.PictureBox()
        Me.Picture6 = New System.Windows.Forms.PictureBox()
        Me.Picture2 = New System.Windows.Forms.PictureBox()
        Me.Picture1 = New System.Windows.Forms.PictureBox()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        CType(Me.PictureSonido, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Picture7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Picture8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Picture9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Picture3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Picture4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Picture5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Picture6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Picture2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Picture1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LabelTitle
        '
        Me.LabelTitle.AutoSize = True
        Me.LabelTitle.Font = New System.Drawing.Font("Segoe UI Semibold", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelTitle.ForeColor = System.Drawing.Color.DimGray
        Me.LabelTitle.Location = New System.Drawing.Point(355, 47)
        Me.LabelTitle.Name = "LabelTitle"
        Me.LabelTitle.Size = New System.Drawing.Size(95, 25)
        Me.LabelTitle.TabIndex = 0
        Me.LabelTitle.Text = "Marcador"
        '
        'LabelJ1
        '
        Me.LabelJ1.AutoSize = True
        Me.LabelJ1.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelJ1.ForeColor = System.Drawing.Color.DimGray
        Me.LabelJ1.Location = New System.Drawing.Point(333, 89)
        Me.LabelJ1.Name = "LabelJ1"
        Me.LabelJ1.Size = New System.Drawing.Size(0, 25)
        Me.LabelJ1.TabIndex = 2
        '
        'LabelJ2
        '
        Me.LabelJ2.AutoSize = True
        Me.LabelJ2.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelJ2.ForeColor = System.Drawing.Color.DimGray
        Me.LabelJ2.Location = New System.Drawing.Point(333, 123)
        Me.LabelJ2.Name = "LabelJ2"
        Me.LabelJ2.Size = New System.Drawing.Size(0, 25)
        Me.LabelJ2.TabIndex = 3
        '
        'LabelLoad
        '
        Me.LabelLoad.BackColor = System.Drawing.Color.Transparent
        Me.LabelLoad.Font = New System.Drawing.Font("Segoe UI Semibold", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelLoad.ForeColor = System.Drawing.Color.DodgerBlue
        Me.LabelLoad.Location = New System.Drawing.Point(333, 231)
        Me.LabelLoad.Name = "LabelLoad"
        Me.LabelLoad.Size = New System.Drawing.Size(150, 30)
        Me.LabelLoad.TabIndex = 7
        Me.LabelLoad.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'LabelM1
        '
        Me.LabelM1.AutoSize = True
        Me.LabelM1.BackColor = System.Drawing.Color.Transparent
        Me.LabelM1.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelM1.ForeColor = System.Drawing.Color.DimGray
        Me.LabelM1.Location = New System.Drawing.Point(446, 89)
        Me.LabelM1.Name = "LabelM1"
        Me.LabelM1.Size = New System.Drawing.Size(22, 25)
        Me.LabelM1.TabIndex = 9
        Me.LabelM1.Text = "0"
        '
        'LabelM2
        '
        Me.LabelM2.AutoSize = True
        Me.LabelM2.BackColor = System.Drawing.Color.Transparent
        Me.LabelM2.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelM2.ForeColor = System.Drawing.Color.DimGray
        Me.LabelM2.Location = New System.Drawing.Point(447, 123)
        Me.LabelM2.Name = "LabelM2"
        Me.LabelM2.Size = New System.Drawing.Size(22, 25)
        Me.LabelM2.TabIndex = 10
        Me.LabelM2.Text = "0"
        '
        'LabelNum
        '
        Me.LabelNum.AutoSize = True
        Me.LabelNum.Location = New System.Drawing.Point(26, 293)
        Me.LabelNum.Name = "LabelNum"
        Me.LabelNum.Size = New System.Drawing.Size(13, 13)
        Me.LabelNum.TabIndex = 11
        Me.LabelNum.Text = "0"
        Me.LabelNum.Visible = False
        '
        'J1
        '
        Me.J1.AutoSize = True
        Me.J1.Location = New System.Drawing.Point(44, 293)
        Me.J1.Name = "J1"
        Me.J1.Size = New System.Drawing.Size(13, 13)
        Me.J1.TabIndex = 21
        Me.J1.Text = "0"
        Me.J1.Visible = False
        '
        'J2
        '
        Me.J2.AutoSize = True
        Me.J2.Location = New System.Drawing.Point(59, 293)
        Me.J2.Name = "J2"
        Me.J2.Size = New System.Drawing.Size(13, 13)
        Me.J2.TabIndex = 22
        Me.J2.Text = "0"
        Me.J2.Visible = False
        '
        'LabelStart
        '
        Me.LabelStart.AutoSize = True
        Me.LabelStart.Location = New System.Drawing.Point(75, 293)
        Me.LabelStart.Name = "LabelStart"
        Me.LabelStart.Size = New System.Drawing.Size(13, 13)
        Me.LabelStart.TabIndex = 24
        Me.LabelStart.Text = "0"
        Me.LabelStart.Visible = False
        '
        'LabelEstado
        '
        Me.LabelEstado.AutoSize = True
        Me.LabelEstado.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelEstado.ForeColor = System.Drawing.Color.DodgerBlue
        Me.LabelEstado.Location = New System.Drawing.Point(9, 285)
        Me.LabelEstado.Name = "LabelEstado"
        Me.LabelEstado.Size = New System.Drawing.Size(0, 25)
        Me.LabelEstado.TabIndex = 25
        '
        'LabelName1
        '
        Me.LabelName1.AutoSize = True
        Me.LabelName1.Location = New System.Drawing.Point(109, 293)
        Me.LabelName1.Name = "LabelName1"
        Me.LabelName1.Size = New System.Drawing.Size(13, 13)
        Me.LabelName1.TabIndex = 38
        Me.LabelName1.Text = "0"
        Me.LabelName1.Visible = False
        '
        'LabelName2
        '
        Me.LabelName2.AutoSize = True
        Me.LabelName2.Location = New System.Drawing.Point(92, 293)
        Me.LabelName2.Name = "LabelName2"
        Me.LabelName2.Size = New System.Drawing.Size(13, 13)
        Me.LabelName2.TabIndex = 39
        Me.LabelName2.Text = "0"
        Me.LabelName2.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Segoe UI Semibold", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.DodgerBlue
        Me.Label1.Location = New System.Drawing.Point(356, 203)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(104, 30)
        Me.Label1.TabIndex = 40
        Me.Label1.Text = "Turno de "
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.DodgerBlue
        Me.Label2.Location = New System.Drawing.Point(7, 3)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(59, 20)
        Me.Label2.TabIndex = 53
        Me.Label2.Text = "Partida:"
        '
        'LabelPartida
        '
        Me.LabelPartida.AutoSize = True
        Me.LabelPartida.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelPartida.ForeColor = System.Drawing.Color.DodgerBlue
        Me.LabelPartida.Location = New System.Drawing.Point(61, 3)
        Me.LabelPartida.Name = "LabelPartida"
        Me.LabelPartida.Size = New System.Drawing.Size(17, 20)
        Me.LabelPartida.TabIndex = 54
        Me.LabelPartida.Text = "1"
        '
        'PictureSonido
        '
        Me.PictureSonido.Image = Global.Tic_tac_toe.My.Resources.Resources.sonido
        Me.PictureSonido.Location = New System.Drawing.Point(468, 5)
        Me.PictureSonido.Name = "PictureSonido"
        Me.PictureSonido.Size = New System.Drawing.Size(30, 30)
        Me.PictureSonido.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureSonido.TabIndex = 37
        Me.PictureSonido.TabStop = False
        '
        'Picture7
        '
        Me.Picture7.BackColor = System.Drawing.Color.White
        Me.Picture7.Location = New System.Drawing.Point(25, 211)
        Me.Picture7.Name = "Picture7"
        Me.Picture7.Size = New System.Drawing.Size(76, 76)
        Me.Picture7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Picture7.TabIndex = 20
        Me.Picture7.TabStop = False
        '
        'Picture8
        '
        Me.Picture8.BackColor = System.Drawing.Color.White
        Me.Picture8.Location = New System.Drawing.Point(115, 211)
        Me.Picture8.Name = "Picture8"
        Me.Picture8.Size = New System.Drawing.Size(76, 76)
        Me.Picture8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Picture8.TabIndex = 19
        Me.Picture8.TabStop = False
        '
        'Picture9
        '
        Me.Picture9.BackColor = System.Drawing.Color.White
        Me.Picture9.Location = New System.Drawing.Point(205, 211)
        Me.Picture9.Name = "Picture9"
        Me.Picture9.Size = New System.Drawing.Size(76, 76)
        Me.Picture9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Picture9.TabIndex = 18
        Me.Picture9.TabStop = False
        '
        'Picture3
        '
        Me.Picture3.BackColor = System.Drawing.Color.White
        Me.Picture3.Location = New System.Drawing.Point(205, 31)
        Me.Picture3.Name = "Picture3"
        Me.Picture3.Size = New System.Drawing.Size(76, 76)
        Me.Picture3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Picture3.TabIndex = 17
        Me.Picture3.TabStop = False
        '
        'Picture4
        '
        Me.Picture4.BackColor = System.Drawing.Color.White
        Me.Picture4.Location = New System.Drawing.Point(25, 121)
        Me.Picture4.Name = "Picture4"
        Me.Picture4.Size = New System.Drawing.Size(76, 76)
        Me.Picture4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Picture4.TabIndex = 16
        Me.Picture4.TabStop = False
        '
        'Picture5
        '
        Me.Picture5.BackColor = System.Drawing.Color.White
        Me.Picture5.Location = New System.Drawing.Point(115, 121)
        Me.Picture5.Name = "Picture5"
        Me.Picture5.Size = New System.Drawing.Size(76, 76)
        Me.Picture5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Picture5.TabIndex = 15
        Me.Picture5.TabStop = False
        '
        'Picture6
        '
        Me.Picture6.BackColor = System.Drawing.Color.White
        Me.Picture6.Location = New System.Drawing.Point(205, 121)
        Me.Picture6.Name = "Picture6"
        Me.Picture6.Size = New System.Drawing.Size(76, 76)
        Me.Picture6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Picture6.TabIndex = 14
        Me.Picture6.TabStop = False
        '
        'Picture2
        '
        Me.Picture2.BackColor = System.Drawing.Color.White
        Me.Picture2.Location = New System.Drawing.Point(115, 31)
        Me.Picture2.Name = "Picture2"
        Me.Picture2.Size = New System.Drawing.Size(76, 76)
        Me.Picture2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Picture2.TabIndex = 13
        Me.Picture2.TabStop = False
        '
        'Picture1
        '
        Me.Picture1.BackColor = System.Drawing.Color.White
        Me.Picture1.Location = New System.Drawing.Point(25, 31)
        Me.Picture1.Name = "Picture1"
        Me.Picture1.Size = New System.Drawing.Size(76, 76)
        Me.Picture1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Picture1.TabIndex = 12
        Me.Picture1.TabStop = False
        '
        'PictureBox4
        '
        Me.PictureBox4.Image = Global.Tic_tac_toe.My.Resources.Resources.hlinea
        Me.PictureBox4.Location = New System.Drawing.Point(23, 199)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(260, 10)
        Me.PictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox4.TabIndex = 6
        Me.PictureBox4.TabStop = False
        '
        'PictureBox3
        '
        Me.PictureBox3.Image = Global.Tic_tac_toe.My.Resources.Resources.hlinea
        Me.PictureBox3.Location = New System.Drawing.Point(23, 109)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(260, 10)
        Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox3.TabIndex = 5
        Me.PictureBox3.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = Global.Tic_tac_toe.My.Resources.Resources.linea
        Me.PictureBox2.Location = New System.Drawing.Point(193, 29)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(10, 260)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 4
        Me.PictureBox2.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.Tic_tac_toe.My.Resources.Resources.linea
        Me.PictureBox1.Location = New System.Drawing.Point(103, 30)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(10, 260)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 1
        Me.PictureBox1.TabStop = False
        '
        'Home
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(504, 311)
        Me.Controls.Add(Me.LabelPartida)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.LabelName2)
        Me.Controls.Add(Me.LabelName1)
        Me.Controls.Add(Me.PictureSonido)
        Me.Controls.Add(Me.LabelEstado)
        Me.Controls.Add(Me.LabelStart)
        Me.Controls.Add(Me.J2)
        Me.Controls.Add(Me.J1)
        Me.Controls.Add(Me.Picture7)
        Me.Controls.Add(Me.Picture8)
        Me.Controls.Add(Me.Picture9)
        Me.Controls.Add(Me.Picture3)
        Me.Controls.Add(Me.Picture4)
        Me.Controls.Add(Me.Picture5)
        Me.Controls.Add(Me.Picture6)
        Me.Controls.Add(Me.Picture2)
        Me.Controls.Add(Me.Picture1)
        Me.Controls.Add(Me.LabelNum)
        Me.Controls.Add(Me.LabelM2)
        Me.Controls.Add(Me.LabelM1)
        Me.Controls.Add(Me.LabelLoad)
        Me.Controls.Add(Me.PictureBox4)
        Me.Controls.Add(Me.PictureBox3)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.LabelJ2)
        Me.Controls.Add(Me.LabelJ1)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.LabelTitle)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Home"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Tag = ""
        Me.Text = "Tic tac toe"
        CType(Me.PictureSonido, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Picture7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Picture8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Picture9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Picture3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Picture4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Picture5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Picture6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Picture2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Picture1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents LabelTitle As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents LabelJ1 As System.Windows.Forms.Label
    Friend WithEvents LabelJ2 As System.Windows.Forms.Label
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents LabelLoad As System.Windows.Forms.Label
    Friend WithEvents LabelM1 As System.Windows.Forms.Label
    Friend WithEvents LabelM2 As System.Windows.Forms.Label
    Friend WithEvents LabelNum As System.Windows.Forms.Label
    Friend WithEvents Picture1 As System.Windows.Forms.PictureBox
    Friend WithEvents Picture2 As System.Windows.Forms.PictureBox
    Friend WithEvents Picture6 As System.Windows.Forms.PictureBox
    Friend WithEvents Picture5 As System.Windows.Forms.PictureBox
    Friend WithEvents Picture4 As System.Windows.Forms.PictureBox
    Friend WithEvents Picture3 As System.Windows.Forms.PictureBox
    Friend WithEvents Picture9 As System.Windows.Forms.PictureBox
    Friend WithEvents Picture8 As System.Windows.Forms.PictureBox
    Friend WithEvents Picture7 As System.Windows.Forms.PictureBox
    Friend WithEvents J1 As System.Windows.Forms.Label
    Friend WithEvents J2 As System.Windows.Forms.Label
    Friend WithEvents LabelStart As System.Windows.Forms.Label
    Friend WithEvents LabelEstado As System.Windows.Forms.Label
    Friend WithEvents PictureSonido As System.Windows.Forms.PictureBox
    Friend WithEvents LabelName1 As System.Windows.Forms.Label
    Friend WithEvents LabelName2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents LabelPartida As System.Windows.Forms.Label

End Class
