﻿Public Class Home
    Dim p, r As String 'Sonido
    Dim s, t As System.Media.SoundPlayer 'Sonido
    Dim m(8) As Integer 'Arreglo que contiene las jugadas en 1 y 2
    Dim h As Integer 'Contador Humano
    Dim c As Integer 'Contador CPU
    Dim take As Integer 'Numero de casilla a tirar por el CPU
    Dim tiros As Integer 'Centinela para solo tirar una vez CPU

    Private Sub Home_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        take = 0
        tiros = 0
        LabelLoad.TextAlign = ContentAlignment.BottomCenter
        p = My.Application.Info.DirectoryPath
        s = New System.Media.SoundPlayer(p & "\click.wav")
        r = My.Application.Info.DirectoryPath
        t = New System.Media.SoundPlayer(p & "\nuevo.wav")
        If My.Computer.FileSystem.FileExists("sonido.txt") Then
            PictureSonido.Image = My.Resources.sonido
        Else
            PictureSonido.Image = My.Resources.silencio
        End If
        LabelJ1.Text = LabelName1.Text & ":"
        LabelJ2.Text = LabelName2.Text & ":"
        If TimeOfDay.Second Mod 2 = 0 Then
            LabelStart.Text = "2"
            If LabelNum.Text = 1 Then
                LabelLoad.Text = LabelName2.Text
                Call CPU()
            End If
            If LabelNum.Text = 2 Then
                LabelLoad.Text = LabelName2.Text
            End If
        Else
            LabelStart.Text = "1"
            If LabelNum.Text = 1 Then
                LabelLoad.Text = LabelName1.Text
            End If
            If LabelNum.Text = 2 Then
                LabelLoad.Text = LabelName1.Text
            End If
        End If
    End Sub

    Private Sub Picture1_Click(sender As Object, e As EventArgs) Handles Picture1.Click
        If My.Computer.FileSystem.FileExists("sonido.txt") Then
            s.Play()
        End If
        If LabelStart.Text = 1 Then
            m(0) = 1
            LabelStart.Text = 2
            If J1.Text = "x" Then
                Picture1.Image = My.Resources.tache
            End If
            If J1.Text = "o" Then
                Picture1.Image = My.Resources.circulo
            End If
            If LabelNum.Text = 1 Then
                LabelLoad.Text = LabelName2.Text
                Call CPU()
            End If
            If LabelNum.Text = 2 Then
                LabelLoad.Text = LabelName2.Text
            End If
            Picture1.Enabled = False
            Call Estado()
        Else
            m(0) = 2
            LabelStart.Text = 1
            If LabelNum.Text = 1 Or LabelNum.Text = 2 Then
                LabelLoad.Text = LabelName1.Text
            End If
            If J2.Text = "x" Then
                Picture1.Image = My.Resources.tache
            End If
            If J2.Text = "o" Then
                Picture1.Image = My.Resources.circulo
            End If
            Picture1.Enabled = False
            Call Estado()
        End If
    End Sub

    Private Sub Picture1_MouseEnter(sender As Object, e As EventArgs) Handles Picture1.MouseEnter
        Picture1.BackColor = Color.FromArgb(235, 235, 235)
    End Sub

    Private Sub Picture1_MouseLeave(sender As Object, e As EventArgs) Handles Picture1.MouseLeave
        Picture1.BackColor = Color.White
    End Sub

    Private Sub Picture2_Click(sender As Object, e As EventArgs) Handles Picture2.Click
        If My.Computer.FileSystem.FileExists("sonido.txt") Then
            s.Play()
        End If
        If LabelStart.Text = 1 Then
            m(1) = 1
            LabelStart.Text = 2
            If J1.Text = "x" Then
                Picture2.Image = My.Resources.tache
            End If
            If J1.Text = "o" Then
                Picture2.Image = My.Resources.circulo
            End If
            If LabelNum.Text = 1 Then
                LabelLoad.Text = LabelName2.Text
                Call CPU()
            End If
            If LabelNum.Text = 2 Then
                LabelLoad.Text = LabelName2.Text
            End If
            Picture2.Enabled = False
            Call Estado()
        Else
            m(1) = 2
            LabelStart.Text = 1
            If LabelNum.Text = 1 Or LabelNum.Text = 2 Then
                LabelLoad.Text = LabelName1.Text
            End If
            If J2.Text = "x" Then
                Picture2.Image = My.Resources.tache
            End If
            If J2.Text = "o" Then
                Picture2.Image = My.Resources.circulo
            End If
            Picture2.Enabled = False
            Call Estado()
        End If
    End Sub

    Private Sub Picture2_MouseEnter(sender As Object, e As EventArgs) Handles Picture2.MouseEnter
        Picture2.BackColor = Color.FromArgb(235, 235, 235)
    End Sub

    Private Sub Picture2_MouseLeave(sender As Object, e As EventArgs) Handles Picture2.MouseLeave
        Picture2.BackColor = Color.White
    End Sub

    Private Sub Picture3_Click(sender As Object, e As EventArgs) Handles Picture3.Click
        If My.Computer.FileSystem.FileExists("sonido.txt") Then
            s.Play()
        End If
        If LabelStart.Text = 1 Then
            m(2) = 1
            LabelStart.Text = 2
            If J1.Text = "x" Then
                Picture3.Image = My.Resources.tache
            End If
            If J1.Text = "o" Then
                Picture3.Image = My.Resources.circulo
            End If
            If LabelNum.Text = 1 Then
                LabelLoad.Text = LabelName2.Text
                Call CPU()
            End If
            If LabelNum.Text = 2 Then
                LabelLoad.Text = LabelName2.Text
            End If
            Picture3.Enabled = False
            Call Estado()
        Else
            m(2) = 2
            LabelStart.Text = 1
            If LabelNum.Text = 1 Or LabelNum.Text = 2 Then
                LabelLoad.Text = LabelName1.Text
            End If
            If J2.Text = "x" Then
                Picture3.Image = My.Resources.tache
            End If
            If J2.Text = "o" Then
                Picture3.Image = My.Resources.circulo
            End If
            Picture3.Enabled = False
            Call Estado()
        End If
    End Sub

    Private Sub Picture3_MouseEnter(sender As Object, e As EventArgs) Handles Picture3.MouseEnter
        Picture3.BackColor = Color.FromArgb(235, 235, 235)
    End Sub

    Private Sub Picture3_MouseLeave(sender As Object, e As EventArgs) Handles Picture3.MouseLeave
        Picture3.BackColor = Color.White
    End Sub

    Private Sub Picture4_Click(sender As Object, e As EventArgs) Handles Picture4.Click
        If My.Computer.FileSystem.FileExists("sonido.txt") Then
            s.Play()
        End If
        If LabelStart.Text = 1 Then
            m(3) = 1
            LabelStart.Text = 2
            If J1.Text = "x" Then
                Picture4.Image = My.Resources.tache
            End If
            If J1.Text = "o" Then
                Picture4.Image = My.Resources.circulo
            End If
            If LabelNum.Text = 1 Then
                LabelLoad.Text = LabelName2.Text
                Call CPU()
            End If
            If LabelNum.Text = 2 Then
                LabelLoad.Text = LabelName2.Text
            End If
            Picture4.Enabled = False
            Call Estado()
        Else
            m(3) = 2
            LabelStart.Text = 1
            If LabelNum.Text = 1 Or LabelNum.Text = 2 Then
                LabelLoad.Text = LabelName1.Text
            End If
            If J2.Text = "x" Then
                Picture4.Image = My.Resources.tache
            End If
            If J2.Text = "o" Then
                Picture4.Image = My.Resources.circulo
            End If
            Picture4.Enabled = False
            Call Estado()
        End If
    End Sub

    Private Sub Picture4_MouseEnter(sender As Object, e As EventArgs) Handles Picture4.MouseEnter
        Picture4.BackColor = Color.FromArgb(235, 235, 235)
    End Sub

    Private Sub Picture4_MouseLeave(sender As Object, e As EventArgs) Handles Picture4.MouseLeave
        Picture4.BackColor = Color.White
    End Sub

    Private Sub Picture5_Click(sender As Object, e As EventArgs) Handles Picture5.Click
        If My.Computer.FileSystem.FileExists("sonido.txt") Then
            s.Play()
        End If
        If LabelStart.Text = 1 Then
            m(4) = 1
            LabelStart.Text = 2
            If J1.Text = "x" Then
                Picture5.Image = My.Resources.tache
            End If
            If J1.Text = "o" Then
                Picture5.Image = My.Resources.circulo
            End If
            If LabelNum.Text = 1 Then
                LabelLoad.Text = LabelName2.Text
                Call CPU()
            End If
            If LabelNum.Text = 2 Then
                LabelLoad.Text = LabelName2.Text
            End If
            Picture5.Enabled = False
            Call Estado()
        Else
            m(4) = 2
            LabelStart.Text = 1
            If LabelNum.Text = 1 Or LabelNum.Text = 2 Then
                LabelLoad.Text = LabelName1.Text
            End If
            If J2.Text = "x" Then
                Picture5.Image = My.Resources.tache
            End If
            If J2.Text = "o" Then
                Picture5.Image = My.Resources.circulo
            End If
            Picture5.Enabled = False
            Call Estado()
        End If
    End Sub

    Private Sub Picture5_MouseEnter(sender As Object, e As EventArgs) Handles Picture5.MouseEnter
        Picture5.BackColor = Color.FromArgb(235, 235, 235)
    End Sub

    Private Sub Picture5_MouseLeave(sender As Object, e As EventArgs) Handles Picture5.MouseLeave
        Picture5.BackColor = Color.White
    End Sub

    Private Sub Picture6_Click(sender As Object, e As EventArgs) Handles Picture6.Click
        If My.Computer.FileSystem.FileExists("sonido.txt") Then
            s.Play()
        End If
        If LabelStart.Text = 1 Then
            m(5) = 1
            LabelStart.Text = 2
            If J1.Text = "x" Then
                Picture6.Image = My.Resources.tache
            End If
            If J1.Text = "o" Then
                Picture6.Image = My.Resources.circulo
            End If
            If LabelNum.Text = 1 Then
                LabelLoad.Text = LabelName2.Text
                Call CPU()
            End If
            If LabelNum.Text = 2 Then
                LabelLoad.Text = LabelName2.Text
            End If
            Picture6.Enabled = False
            Call Estado()
        Else
            m(5) = 2
            LabelStart.Text = 1
            If LabelNum.Text = 1 Or LabelNum.Text = 2 Then
                LabelLoad.Text = LabelName1.Text
            End If
            If J2.Text = "x" Then
                Picture6.Image = My.Resources.tache
            End If
            If J2.Text = "o" Then
                Picture6.Image = My.Resources.circulo
            End If
            Picture6.Enabled = False
            Call Estado()
        End If
    End Sub

    Private Sub Picture6_MouseEnter(sender As Object, e As EventArgs) Handles Picture6.MouseEnter
        Picture6.BackColor = Color.FromArgb(235, 235, 235)
    End Sub

    Private Sub Picture6_MouseLeave(sender As Object, e As EventArgs) Handles Picture6.MouseLeave
        Picture6.BackColor = Color.White
    End Sub

    Private Sub Picture7_Click(sender As Object, e As EventArgs) Handles Picture7.Click
        If My.Computer.FileSystem.FileExists("sonido.txt") Then
            s.Play()
        End If
        If LabelStart.Text = 1 Then
            m(6) = 1
            LabelStart.Text = 2
            If J1.Text = "x" Then
                Picture7.Image = My.Resources.tache
            End If
            If J1.Text = "o" Then
                Picture7.Image = My.Resources.circulo
            End If
            If LabelNum.Text = 1 Then
                LabelLoad.Text = LabelName2.Text
                Call CPU()
            End If
            If LabelNum.Text = 2 Then
                LabelLoad.Text = LabelName2.Text
            End If
            Picture7.Enabled = False
            Call Estado()
        Else
            m(6) = 2
            LabelStart.Text = 1
            If LabelNum.Text = 1 Or LabelNum.Text = 2 Then
                LabelLoad.Text = LabelName1.Text
            End If
            If J2.Text = "x" Then
                Picture7.Image = My.Resources.tache
            End If
            If J2.Text = "o" Then
                Picture7.Image = My.Resources.circulo
            End If
            Picture7.Enabled = False
            Call Estado()
        End If
    End Sub

    Private Sub Picture7_MouseEnter(sender As Object, e As EventArgs) Handles Picture7.MouseEnter
        Picture7.BackColor = Color.FromArgb(235, 235, 235)
    End Sub

    Private Sub Picture7_MouseLeave(sender As Object, e As EventArgs) Handles Picture7.MouseLeave
        Picture7.BackColor = Color.White
    End Sub

    Private Sub Picture8_Click(sender As Object, e As EventArgs) Handles Picture8.Click
        If My.Computer.FileSystem.FileExists("sonido.txt") Then
            s.Play()
        End If
        If LabelStart.Text = 1 Then
            m(7) = 1
            LabelStart.Text = 2
            If J1.Text = "x" Then
                Picture8.Image = My.Resources.tache
            End If
            If J1.Text = "o" Then
                Picture8.Image = My.Resources.circulo
            End If
            If LabelNum.Text = 1 Then
                LabelLoad.Text = LabelName2.Text
                Call CPU()
            End If
            If LabelNum.Text = 2 Then
                LabelLoad.Text = LabelName2.Text
            End If
            Picture8.Enabled = False
            Call Estado()
        Else
            m(7) = 2
            LabelStart.Text = 1
            If LabelNum.Text = 1 Or LabelNum.Text = 2 Then
                LabelLoad.Text = LabelName1.Text
            End If
            If J2.Text = "x" Then
                Picture8.Image = My.Resources.tache
            End If
            If J2.Text = "o" Then
                Picture8.Image = My.Resources.circulo
            End If
            Picture8.Enabled = False
            Call Estado()
        End If
    End Sub

    Private Sub Picture8_MouseEnter(sender As Object, e As EventArgs) Handles Picture8.MouseEnter
        Picture8.BackColor = Color.FromArgb(235, 235, 235)
    End Sub

    Private Sub Picture8_MouseLeave(sender As Object, e As EventArgs) Handles Picture8.MouseLeave
        Picture8.BackColor = Color.White
    End Sub

    Private Sub Picture9_Click(sender As Object, e As EventArgs) Handles Picture9.Click
        If My.Computer.FileSystem.FileExists("sonido.txt") Then
            s.Play()
        End If
        If LabelStart.Text = 1 Then
            m(8) = 1
            LabelStart.Text = 2
            If J1.Text = "x" Then
                Picture9.Image = My.Resources.tache
            End If
            If J1.Text = "o" Then
                Picture9.Image = My.Resources.circulo
            End If
            If LabelNum.Text = 1 Then
                LabelLoad.Text = LabelName2.Text
                Call CPU()
            End If
            If LabelNum.Text = 2 Then
                LabelLoad.Text = LabelName2.Text
            End If
            Picture9.Enabled = False
            Call Estado()
        Else
            m(8) = 2
            LabelStart.Text = 1
            If LabelNum.Text = 1 Or LabelNum.Text = 2 Then
                LabelLoad.Text = LabelName1.Text
            End If
            If J2.Text = "x" Then
                Picture9.Image = My.Resources.tache
            End If
            If J2.Text = "o" Then
                Picture9.Image = My.Resources.circulo
            End If
            Picture9.Enabled = False
            Call Estado()
        End If
    End Sub

    Private Sub Picture9_MouseEnter(sender As Object, e As EventArgs) Handles Picture9.MouseEnter
        Picture9.BackColor = Color.FromArgb(235, 235, 235)
    End Sub

    Private Sub Picture9_MouseLeave(sender As Object, e As EventArgs) Handles Picture9.MouseLeave
        Picture9.BackColor = Color.White
    End Sub

    Private Sub PictureSonido_Click(sender As Object, e As EventArgs) Handles PictureSonido.Click
        If My.Computer.FileSystem.FileExists("sonido.txt") Then
            My.Computer.FileSystem.DeleteFile("sonido.txt")
            PictureSonido.Image = My.Resources.silencio
        Else
            Dim sound As String = Application.StartupPath & "/sonido.txt"
            My.Computer.FileSystem.WriteAllText(sound, LabelNum.Text, False)
            PictureSonido.Image = My.Resources.sonido
        End If
    End Sub

    Private Sub Estado()
        If m(0) = 0 Then
            Picture1.Enabled = True
        End If
        If m(1) = 0 Then
            Picture2.Enabled = True
        End If
        If m(2) = 0 Then
            Picture3.Enabled = True
        End If
        If m(3) = 0 Then
            Picture4.Enabled = True
        End If
        If m(4) = 0 Then
            Picture5.Enabled = True
        End If
        If m(5) = 0 Then
            Picture6.Enabled = True
        End If
        If m(6) = 0 Then
            Picture7.Enabled = True
        End If
        If m(7) = 0 Then
            Picture8.Enabled = True
        End If
        If m(8) = 0 Then
            Picture9.Enabled = True
        End If
        If m(0) = 1 And m(1) = 1 And m(2) = 1 Then
            If J1.Text = "x" Then
                Picture1.Image = My.Resources.gtache
                Picture2.Image = My.Resources.gtache
                Picture3.Image = My.Resources.gtache
            End If
            If J1.Text = "o" Then
                Picture1.Image = My.Resources.gcirculo
                Picture2.Image = My.Resources.gcirculo
                Picture3.Image = My.Resources.gcirculo
            End If
            MsgBox(LabelName1.Text & " ha ganado.", MsgBoxStyle.Information, "Tic tac toe")
            LabelM1.Text = LabelM1.Text + 1
            Call Barrer()
        End If
        If m(0) = 2 And m(1) = 2 And m(2) = 2 Then
            If J2.Text = "x" Then
                Picture1.Image = My.Resources.gtache
                Picture2.Image = My.Resources.gtache
                Picture3.Image = My.Resources.gtache
            End If
            If J2.Text = "o" Then
                Picture1.Image = My.Resources.gcirculo
                Picture2.Image = My.Resources.gcirculo
                Picture3.Image = My.Resources.gcirculo
            End If
            MsgBox(LabelName2.Text & " ha ganado.", MsgBoxStyle.Information, "Tic tac toe")
            LabelM2.Text = LabelM2.Text + 1
            Call Barrer()
        End If
        If m(3) = 1 And m(4) = 1 And m(5) = 1 Then
            If J1.Text = "x" Then
                Picture4.Image = My.Resources.gtache
                Picture5.Image = My.Resources.gtache
                Picture6.Image = My.Resources.gtache
            End If
            If J1.Text = "o" Then
                Picture4.Image = My.Resources.gcirculo
                Picture5.Image = My.Resources.gcirculo
                Picture6.Image = My.Resources.gcirculo
            End If
            MsgBox(LabelName1.Text & " ha ganado.", MsgBoxStyle.Information, "Tic tac toe")
            LabelM1.Text = LabelM1.Text + 1
            Call Barrer()
        End If
        If m(3) = 2 And m(4) = 2 And m(5) = 2 Then
            If J2.Text = "x" Then
                Picture4.Image = My.Resources.gtache
                Picture5.Image = My.Resources.gtache
                Picture6.Image = My.Resources.gtache
            End If
            If J2.Text = "o" Then
                Picture4.Image = My.Resources.gcirculo
                Picture5.Image = My.Resources.gcirculo
                Picture6.Image = My.Resources.gcirculo
            End If
            MsgBox(LabelName2.Text & " ha ganado.", MsgBoxStyle.Information, "Tic tac toe")
            LabelM2.Text = LabelM2.Text + 1
            Call Barrer()
        End If
        If m(6) = 1 And m(7) = 1 And m(8) = 1 Then
            If J1.Text = "x" Then
                Picture7.Image = My.Resources.gtache
                Picture8.Image = My.Resources.gtache
                Picture9.Image = My.Resources.gtache
            End If
            If J1.Text = "o" Then
                Picture7.Image = My.Resources.gcirculo
                Picture8.Image = My.Resources.gcirculo
                Picture9.Image = My.Resources.gcirculo
            End If
            MsgBox(LabelName1.Text & " ha ganado.", MsgBoxStyle.Information, "Tic tac toe")
            LabelM1.Text = LabelM1.Text + 1
            Call Barrer()
        End If
        If m(6) = 2 And m(7) = 2 And m(8) = 2 Then
            If J2.Text = "x" Then
                Picture7.Image = My.Resources.gtache
                Picture8.Image = My.Resources.gtache
                Picture9.Image = My.Resources.gtache
            End If
            If J2.Text = "o" Then
                Picture7.Image = My.Resources.gcirculo
                Picture8.Image = My.Resources.gcirculo
                Picture9.Image = My.Resources.gcirculo
            End If
            MsgBox(LabelName2.Text & " ha ganado.", MsgBoxStyle.Information, "Tic tac toe")
            LabelM2.Text = LabelM2.Text + 1
            Call Barrer()
        End If
        If m(0) = 1 And m(3) = 1 And m(6) = 1 Then
            If J1.Text = "x" Then
                Picture1.Image = My.Resources.gtache
                Picture4.Image = My.Resources.gtache
                Picture7.Image = My.Resources.gtache
            End If
            If J1.Text = "o" Then
                Picture1.Image = My.Resources.gcirculo
                Picture4.Image = My.Resources.gcirculo
                Picture7.Image = My.Resources.gcirculo
            End If
            MsgBox(LabelName1.Text & " ha ganado.", MsgBoxStyle.Information, "Tic tac toe")
            LabelM1.Text = LabelM1.Text + 1
            Call Barrer()
        End If
        If m(0) = 2 And m(3) = 2 And m(6) = 2 Then
            If J2.Text = "x" Then
                Picture1.Image = My.Resources.gtache
                Picture4.Image = My.Resources.gtache
                Picture7.Image = My.Resources.gtache
            End If
            If J2.Text = "o" Then
                Picture1.Image = My.Resources.gcirculo
                Picture4.Image = My.Resources.gcirculo
                Picture7.Image = My.Resources.gcirculo
            End If
            MsgBox(LabelName2.Text & " ha ganado.", MsgBoxStyle.Information, "Tic tac toe")
            LabelM2.Text = LabelM2.Text + 1
            Call Barrer()
        End If
        If m(0) = 1 And m(4) = 1 And m(8) = 1 Then
            If J1.Text = "x" Then
                Picture1.Image = My.Resources.gtache
                Picture5.Image = My.Resources.gtache
                Picture9.Image = My.Resources.gtache
            End If
            If J1.Text = "o" Then
                Picture1.Image = My.Resources.gcirculo
                Picture5.Image = My.Resources.gcirculo
                Picture9.Image = My.Resources.gcirculo
            End If
            MsgBox(LabelName1.Text & " ha ganado.", MsgBoxStyle.Information, "Tic tac toe")
            LabelM1.Text = LabelM1.Text + 1
            Call Barrer()
        End If
        If m(0) = 2 And m(4) = 2 And m(8) = 2 Then
            If J2.Text = "x" Then
                Picture1.Image = My.Resources.gtache
                Picture5.Image = My.Resources.gtache
                Picture9.Image = My.Resources.gtache
            End If
            If J2.Text = "o" Then
                Picture1.Image = My.Resources.gcirculo
                Picture5.Image = My.Resources.gcirculo
                Picture9.Image = My.Resources.gcirculo
            End If
            MsgBox(LabelName2.Text & " ha ganado.", MsgBoxStyle.Information, "Tic tac toe")
            LabelM2.Text = LabelM2.Text + 1
            Call Barrer()
        End If
        If m(2) = 1 And m(4) = 1 And m(6) = 1 Then
            If J1.Text = "x" Then
                Picture3.Image = My.Resources.gtache
                Picture5.Image = My.Resources.gtache
                Picture7.Image = My.Resources.gtache
            End If
            If J1.Text = "o" Then
                Picture3.Image = My.Resources.gcirculo
                Picture5.Image = My.Resources.gcirculo
                Picture7.Image = My.Resources.gcirculo
            End If
            MsgBox(LabelName1.Text & " ha ganado.", MsgBoxStyle.Information, "Tic tac toe")
            LabelM1.Text = LabelM1.Text + 1
            Call Barrer()
        End If
        If m(2) = 2 And m(4) = 2 And m(6) = 2 Then
            If J2.Text = "x" Then
                Picture3.Image = My.Resources.gtache
                Picture5.Image = My.Resources.gtache
                Picture7.Image = My.Resources.gtache
            End If
            If J2.Text = "o" Then
                Picture3.Image = My.Resources.gcirculo
                Picture5.Image = My.Resources.gcirculo
                Picture7.Image = My.Resources.gcirculo
            End If
            MsgBox(LabelName2.Text & " ha ganado.", MsgBoxStyle.Information, "Tic tac toe")
            LabelM2.Text = LabelM2.Text + 1
            Call Barrer()
        End If
        If m(1) = 1 And m(4) = 1 And m(7) = 1 Then
            If J1.Text = "x" Then
                Picture2.Image = My.Resources.gtache
                Picture5.Image = My.Resources.gtache
                Picture8.Image = My.Resources.gtache
            End If
            If J1.Text = "o" Then
                Picture2.Image = My.Resources.gcirculo
                Picture5.Image = My.Resources.gcirculo
                Picture8.Image = My.Resources.gcirculo
            End If
            MsgBox(LabelName1.Text & " ha ganado.", MsgBoxStyle.Information, "Tic tac toe")
            LabelM1.Text = LabelM1.Text + 1
            Call Barrer()
        End If
        If m(1) = 2 And m(4) = 2 And m(7) = 2 Then
            If J2.Text = "x" Then
                Picture2.Image = My.Resources.gtache
                Picture5.Image = My.Resources.gtache
                Picture8.Image = My.Resources.gtache
            End If
            If J2.Text = "o" Then
                Picture2.Image = My.Resources.gcirculo
                Picture5.Image = My.Resources.gcirculo
                Picture8.Image = My.Resources.gcirculo
            End If
            MsgBox(LabelName2.Text & " ha ganado.", MsgBoxStyle.Information, "Tic tac toe")
            LabelM2.Text = LabelM2.Text + 1
            Call Barrer()
        End If
        If m(2) = 1 And m(5) = 1 And m(8) = 1 Then
            If J1.Text = "x" Then
                Picture3.Image = My.Resources.gtache
                Picture6.Image = My.Resources.gtache
                Picture9.Image = My.Resources.gtache
            End If
            If J1.Text = "o" Then
                Picture3.Image = My.Resources.gcirculo
                Picture6.Image = My.Resources.gcirculo
                Picture9.Image = My.Resources.gcirculo
            End If
            MsgBox(LabelName1.Text & " ha ganado.", MsgBoxStyle.Information, "Tic tac toe")
            LabelM1.Text = LabelM1.Text + 1
            Call Barrer()
        End If
        If m(2) = 2 And m(5) = 2 And m(8) = 2 Then
            If J2.Text = "x" Then
                Picture3.Image = My.Resources.gtache
                Picture6.Image = My.Resources.gtache
                Picture9.Image = My.Resources.gtache
            End If
            If J2.Text = "o" Then
                Picture3.Image = My.Resources.gcirculo
                Picture6.Image = My.Resources.gcirculo
                Picture9.Image = My.Resources.gcirculo
            End If
            MsgBox(LabelName2.Text & " ha ganado.", MsgBoxStyle.Information, "Tic tac toe")
            LabelM2.Text = LabelM2.Text + 1
            Call Barrer()
        End If
        If m(0) = 0 Or m(1) = 0 Or m(2) = 0 Or m(3) = 0 Or m(4) = 0 Or m(5) = 0 Or m(6) = 0 Or m(7) = 0 Or m(8) = 0 Then
        Else
            MsgBox("Nadie ganó el juego.", MsgBoxStyle.Information, "Tic tac toe")
            Call Barrer()
        End If
    End Sub

    Private Sub Barrer()
        t.Play()
        For barrerr = 0 To 8 Step 1
            m(barrerr) = 0
        Next
        LabelPartida.Text = LabelPartida.Text + 1
        Picture1.Image = Nothing
        Picture1.BackColor = Color.White
        Picture1.Enabled = True
        Picture2.Image = Nothing
        Picture2.BackColor = Color.White
        Picture2.Enabled = True
        Picture3.Image = Nothing
        Picture3.BackColor = Color.White
        Picture3.Enabled = True
        Picture4.Image = Nothing
        Picture4.BackColor = Color.White
        Picture4.Enabled = True
        Picture5.Image = Nothing
        Picture5.BackColor = Color.White
        Picture5.Enabled = True
        Picture6.Image = Nothing
        Picture6.BackColor = Color.White
        Picture6.Enabled = True
        Picture7.Image = Nothing
        Picture7.BackColor = Color.White
        Picture7.Enabled = True
        Picture8.Image = Nothing
        Picture8.BackColor = Color.White
        Picture8.Enabled = True
        Picture9.Image = Nothing
        Picture9.BackColor = Color.White
        Picture9.Enabled = True
        If TimeOfDay.Second Mod 2 = 0 Then
            LabelStart.Text = "2"
            If LabelNum.Text = 1 Then
                LabelLoad.Text = LabelName2.Text
                Call CPU()
            End If
            If LabelNum.Text = 2 Then
                LabelLoad.Text = LabelName2.Text
            End If
        Else
            LabelStart.Text = "1"
            If LabelNum.Text = 1 Then
                LabelLoad.Text = LabelName1.Text
            End If
            If LabelNum.Text = 2 Then
                LabelLoad.Text = LabelName1.Text
            End If
        End If
    End Sub

    Private Sub CPU()
        tiros = 0
        h = 0
        c = 0
        For i = 0 To 2 Step 1 'Horizontal 1
            If m(i) = 1 Then
                h = h + 1
            End If
            If m(i) = 2 Then
                c = c + 1
            End If
            If c = 2 Then
                For j = 0 To 2 Step 1
                    If m(j) = 0 Then
                        If tiros = 0 Then
                            take = j + 1
                            Call Tirar()
                        End If
                    End If
                Next
            End If
            If h = 2 Then
                For j = 0 To 2 Step 1
                    If m(j) = 0 Then
                        If tiros = 0 Then
                            take = j + 1
                            Call Tirar()
                        End If
                    End If
                Next
            End If
        Next
        h = 0
        c = 0
        For i = 3 To 5 Step 1 'Horizontal 2
            If m(i) = 1 Then
                h = h + 1
            End If
            If m(i) = 2 Then
                c = c + 1
            End If
            If c = 2 Then
                For j = 3 To 5 Step 1
                    If m(j) = 0 Then
                        If tiros = 0 Then
                            take = j + 1
                            Call Tirar()
                        End If
                    End If
                Next
            End If
            If h = 2 Then
                For j = 3 To 5 Step 1
                    If m(j) = 0 Then
                        If tiros = 0 Then
                            take = j + 1
                            Call Tirar()
                        End If
                    End If
                Next
            End If
        Next
        h = 0
        c = 0
        For i = 6 To 8 Step 1 'Horizontal 3
            If m(i) = 1 Then
                h = h + 1
            End If
            If m(i) = 2 Then
                c = c + 1
            End If
            If c = 2 Then
                For j = 6 To 8 Step 1
                    If m(j) = 0 Then
                        If tiros = 0 Then
                            take = j + 1
                            Call Tirar()
                        End If
                    End If
                Next
            End If
            If h = 2 Then
                For j = 6 To 8 Step 1
                    If m(j) = 0 Then
                        If tiros = 0 Then
                            take = j + 1
                            Call Tirar()
                        End If
                    End If
                Next
            End If
        Next
        h = 0
        c = 0
        For i = 0 To 6 Step 3 'Vertical 1
            If m(i) = 1 Then
                h = h + 1
            End If
            If m(i) = 2 Then
                c = c + 1
            End If
            If c = 2 Then
                For j = 0 To 6 Step 3
                    If m(j) = 0 Then
                        If tiros = 0 Then
                            take = j + 1
                            Call Tirar()
                        End If
                    End If
                Next
            End If
            If h = 2 Then
                For j = 0 To 6 Step 3
                    If m(j) = 0 Then
                        If tiros = 0 Then
                            take = j + 1
                            Call Tirar()
                        End If
                    End If
                Next
            End If
        Next
        h = 0
        c = 0
        For i = 1 To 7 Step 3 'Vertical 2
            If m(i) = 1 Then
                h = h + 1
            End If
            If m(i) = 2 Then
                c = c + 1
            End If
            If c = 2 Then
                For j = 1 To 7 Step 3
                    If m(j) = 0 Then
                        If tiros = 0 Then
                            take = j + 1
                            Call Tirar()
                        End If
                    End If
                Next
            End If
            If h = 2 Then
                For j = 1 To 7 Step 3
                    If m(j) = 0 Then
                        If tiros = 0 Then
                            take = j + 1
                            Call Tirar()
                        End If
                    End If
                Next
            End If
        Next
        h = 0
        c = 0
        For i = 2 To 8 Step 3 'Vertical 3
            If m(i) = 1 Then
                h = h + 1
            End If
            If m(i) = 2 Then
                c = c + 1
            End If
            If c = 2 Then
                For j = 2 To 8 Step 3
                    If m(j) = 0 Then
                        If tiros = 0 Then
                            take = j + 1
                            Call Tirar()
                        End If
                    End If
                Next
            End If
            If h = 2 Then
                For j = 2 To 8 Step 3
                    If m(j) = 0 Then
                        If tiros = 0 Then
                            take = j + 1
                            Call Tirar()
                        End If
                    End If
                Next
            End If
        Next
        h = 0
        c = 0
        For i = 0 To 8 Step 4 'Diagonal 1
            If m(i) = 1 Then
                h = h + 1
            End If
            If m(i) = 2 Then
                c = c + 1
            End If
            If c = 2 Then
                For j = 0 To 8 Step 4
                    If m(j) = 0 Then
                        If tiros = 0 Then
                            take = j + 1
                            Call Tirar()
                        End If
                    End If
                Next
            End If
            If h = 2 Then
                For j = 0 To 8 Step 4
                    If m(j) = 0 Then
                        If tiros = 0 Then
                            take = j + 1
                            Call Tirar()
                        End If
                    End If
                Next
            End If
        Next
        h = 0
        c = 0
        For i = 2 To 6 Step 2 'Diagonal 2
            If m(i) = 1 Then
                h = h + 1
            End If
            If m(i) = 2 Then
                c = c + 1
            End If
            If c = 2 Then
                For j = 2 To 6 Step 2
                    If m(j) = 0 Then
                        If tiros = 0 Then
                            take = j + 1
                            Call Tirar()
                        End If
                    End If
                Next
            End If
            If h = 2 Then
                For j = 2 To 6 Step 2
                    If m(j) = 0 Then
                        If tiros = 0 Then
                            take = j + 1
                            Call Tirar()
                        End If
                    End If
                Next
            End If
        Next
        For i = 0 To 8 Step 1 'Tiro normal
            If m(i) = 0 Then
                If tiros = 0 Then
                    take = i + 1
                    Call Tirar()
                End If
            End If
        Next
    End Sub

    Private Sub Tirar()
        tiros = 1
        If take = 1 Then
            Call Picture1_Click(Nothing, Nothing)
        End If
        If take = 2 Then
            Call Picture2_Click(Nothing, Nothing)
        End If
        If take = 3 Then
            Call Picture3_Click(Nothing, Nothing)
        End If
        If take = 4 Then
            Call Picture4_Click(Nothing, Nothing)
        End If
        If take = 5 Then
            Call Picture5_Click(Nothing, Nothing)
        End If
        If take = 6 Then
            Call Picture6_Click(Nothing, Nothing)
        End If
        If take = 7 Then
            Call Picture7_Click(Nothing, Nothing)
        End If
        If take = 8 Then
            Call Picture8_Click(Nothing, Nothing)
        End If
        If take = 9 Then
            Call Picture9_Click(Nothing, Nothing)
        End If
    End Sub

End Class
