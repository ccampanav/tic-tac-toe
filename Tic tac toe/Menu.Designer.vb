﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Menu
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Menu))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ButtonJ1 = New System.Windows.Forms.Button()
        Me.ButtonJ2 = New System.Windows.Forms.Button()
        Me.LabelJ1 = New System.Windows.Forms.Label()
        Me.LabelIns = New System.Windows.Forms.Label()
        Me.LabelJ2 = New System.Windows.Forms.Label()
        Me.Timer = New System.Windows.Forms.Timer(Me.components)
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Picture4 = New System.Windows.Forms.PictureBox()
        Me.Picture3 = New System.Windows.Forms.PictureBox()
        Me.Picture2 = New System.Windows.Forms.PictureBox()
        Me.Picture1 = New System.Windows.Forms.PictureBox()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Picture4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Picture3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Picture2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Picture1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.DodgerBlue
        Me.Label1.Location = New System.Drawing.Point(33, 12)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(267, 37)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Menú  de Tic Tac Toe"
        '
        'ButtonJ1
        '
        Me.ButtonJ1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ButtonJ1.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonJ1.ForeColor = System.Drawing.Color.DimGray
        Me.ButtonJ1.Location = New System.Drawing.Point(67, 79)
        Me.ButtonJ1.Name = "ButtonJ1"
        Me.ButtonJ1.Size = New System.Drawing.Size(200, 44)
        Me.ButtonJ1.TabIndex = 1
        Me.ButtonJ1.Text = "Jugador 1 vs. CPU"
        Me.ButtonJ1.UseVisualStyleBackColor = True
        '
        'ButtonJ2
        '
        Me.ButtonJ2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ButtonJ2.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonJ2.ForeColor = System.Drawing.Color.DimGray
        Me.ButtonJ2.Location = New System.Drawing.Point(67, 150)
        Me.ButtonJ2.Name = "ButtonJ2"
        Me.ButtonJ2.Size = New System.Drawing.Size(200, 44)
        Me.ButtonJ2.TabIndex = 2
        Me.ButtonJ2.Text = "Jugador 1 vs. Jugador 2"
        Me.ButtonJ2.UseVisualStyleBackColor = True
        '
        'LabelJ1
        '
        Me.LabelJ1.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelJ1.ForeColor = System.Drawing.Color.DimGray
        Me.LabelJ1.Location = New System.Drawing.Point(17, 200)
        Me.LabelJ1.Name = "LabelJ1"
        Me.LabelJ1.Size = New System.Drawing.Size(100, 20)
        Me.LabelJ1.TabIndex = 3
        Me.LabelJ1.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.LabelJ1.Visible = False
        '
        'LabelIns
        '
        Me.LabelIns.AutoSize = True
        Me.LabelIns.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelIns.ForeColor = System.Drawing.Color.DodgerBlue
        Me.LabelIns.Location = New System.Drawing.Point(25, 173)
        Me.LabelIns.Name = "LabelIns"
        Me.LabelIns.Size = New System.Drawing.Size(116, 17)
        Me.LabelIns.TabIndex = 4
        Me.LabelIns.Text = "Escoge una figura:"
        Me.LabelIns.Visible = False
        '
        'LabelJ2
        '
        Me.LabelJ2.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelJ2.ForeColor = System.Drawing.Color.DimGray
        Me.LabelJ2.Location = New System.Drawing.Point(17, 236)
        Me.LabelJ2.Name = "LabelJ2"
        Me.LabelJ2.Size = New System.Drawing.Size(100, 20)
        Me.LabelJ2.TabIndex = 5
        Me.LabelJ2.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Timer
        '
        Me.Timer.Interval = 10
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(0, 0)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(335, 6)
        Me.ProgressBar1.TabIndex = 12
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.DodgerBlue
        Me.Label2.Location = New System.Drawing.Point(24, 74)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(118, 17)
        Me.Label2.TabIndex = 13
        Me.Label2.Text = "Escribe tu nombre:"
        Me.Label2.Visible = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.DimGray
        Me.Label3.Location = New System.Drawing.Point(47, 103)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(77, 20)
        Me.Label3.TabIndex = 14
        Me.Label3.Text = "Jugador 1:"
        Me.Label3.Visible = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.DimGray
        Me.Label4.Location = New System.Drawing.Point(47, 138)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(77, 20)
        Me.Label4.TabIndex = 15
        Me.Label4.Text = "Jugador 2:"
        Me.Label4.Visible = False
        '
        'TextBox1
        '
        Me.TextBox1.BackColor = System.Drawing.Color.White
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox1.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.ForeColor = System.Drawing.Color.DimGray
        Me.TextBox1.Location = New System.Drawing.Point(129, 100)
        Me.TextBox1.MaxLength = 10
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(120, 27)
        Me.TextBox1.TabIndex = 16
        Me.TextBox1.Visible = False
        '
        'TextBox2
        '
        Me.TextBox2.BackColor = System.Drawing.Color.White
        Me.TextBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox2.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox2.ForeColor = System.Drawing.Color.DimGray
        Me.TextBox2.Location = New System.Drawing.Point(129, 135)
        Me.TextBox2.MaxLength = 10
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(120, 27)
        Me.TextBox2.TabIndex = 17
        Me.TextBox2.Visible = False
        '
        'PictureBox2
        '
        Me.PictureBox2.Location = New System.Drawing.Point(257, 135)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(25, 25)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 19
        Me.PictureBox2.TabStop = False
        Me.PictureBox2.Visible = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Location = New System.Drawing.Point(257, 100)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(25, 25)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 18
        Me.PictureBox1.TabStop = False
        Me.PictureBox1.Visible = False
        '
        'Picture4
        '
        Me.Picture4.BackColor = System.Drawing.Color.White
        Me.Picture4.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Picture4.Image = Global.Tic_tac_toe.My.Resources.Resources.tache
        Me.Picture4.Location = New System.Drawing.Point(161, 233)
        Me.Picture4.Name = "Picture4"
        Me.Picture4.Size = New System.Drawing.Size(25, 25)
        Me.Picture4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Picture4.TabIndex = 9
        Me.Picture4.TabStop = False
        Me.Picture4.Visible = False
        '
        'Picture3
        '
        Me.Picture3.BackColor = System.Drawing.Color.White
        Me.Picture3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Picture3.Image = Global.Tic_tac_toe.My.Resources.Resources.circulo
        Me.Picture3.Location = New System.Drawing.Point(131, 233)
        Me.Picture3.Name = "Picture3"
        Me.Picture3.Size = New System.Drawing.Size(25, 25)
        Me.Picture3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Picture3.TabIndex = 8
        Me.Picture3.TabStop = False
        Me.Picture3.Visible = False
        '
        'Picture2
        '
        Me.Picture2.BackColor = System.Drawing.Color.White
        Me.Picture2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Picture2.Image = Global.Tic_tac_toe.My.Resources.Resources.tache
        Me.Picture2.Location = New System.Drawing.Point(161, 198)
        Me.Picture2.Name = "Picture2"
        Me.Picture2.Size = New System.Drawing.Size(25, 25)
        Me.Picture2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Picture2.TabIndex = 7
        Me.Picture2.TabStop = False
        Me.Picture2.Visible = False
        '
        'Picture1
        '
        Me.Picture1.BackColor = System.Drawing.Color.White
        Me.Picture1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Picture1.Image = Global.Tic_tac_toe.My.Resources.Resources.circulo
        Me.Picture1.Location = New System.Drawing.Point(131, 198)
        Me.Picture1.Name = "Picture1"
        Me.Picture1.Size = New System.Drawing.Size(25, 25)
        Me.Picture1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Picture1.TabIndex = 6
        Me.Picture1.TabStop = False
        Me.Picture1.Visible = False
        '
        'Menu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(334, 271)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.TextBox2)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.Picture4)
        Me.Controls.Add(Me.Picture3)
        Me.Controls.Add(Me.Picture2)
        Me.Controls.Add(Me.Picture1)
        Me.Controls.Add(Me.LabelJ2)
        Me.Controls.Add(Me.LabelIns)
        Me.Controls.Add(Me.LabelJ1)
        Me.Controls.Add(Me.ButtonJ2)
        Me.Controls.Add(Me.ButtonJ1)
        Me.Controls.Add(Me.Label1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximumSize = New System.Drawing.Size(350, 310)
        Me.MinimumSize = New System.Drawing.Size(350, 310)
        Me.Name = "Menu"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Tic tac toe"
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Picture4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Picture3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Picture2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Picture1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ButtonJ1 As System.Windows.Forms.Button
    Friend WithEvents ButtonJ2 As System.Windows.Forms.Button
    Friend WithEvents LabelJ1 As System.Windows.Forms.Label
    Friend WithEvents LabelIns As System.Windows.Forms.Label
    Friend WithEvents LabelJ2 As System.Windows.Forms.Label
    Friend WithEvents Picture1 As System.Windows.Forms.PictureBox
    Friend WithEvents Picture2 As System.Windows.Forms.PictureBox
    Friend WithEvents Picture3 As System.Windows.Forms.PictureBox
    Friend WithEvents Picture4 As System.Windows.Forms.PictureBox
    Friend WithEvents Timer As System.Windows.Forms.Timer
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
End Class
