﻿Public Class Menu
    Dim t1, t2 As Integer

    Private Sub ButtonJ1_Click(sender As Object, e As EventArgs) Handles ButtonJ1.Click
        If My.Computer.FileSystem.FileExists("sonido.txt") Then
            s.Play()
        End If
        Home.LabelNum.Text = 1
        LabelIns.Visible = True
        LabelJ1.Visible = True
        Label2.Visible = True
        Label3.Visible = True
        TextBox1.Visible = True
        PictureBox1.Visible = True
        t2 = 1
        ButtonJ1.Visible = False
        ButtonJ2.Visible = False
    End Sub

    Private Sub ButtonJ2_Click(sender As Object, e As EventArgs) Handles ButtonJ2.Click
        If My.Computer.FileSystem.FileExists("sonido.txt") Then
            s.Play()
        End If
        Home.LabelNum.Text = 2
        LabelIns.Visible = True
        LabelJ1.Visible = True
        Label2.Visible = True
        Label2.Text = "Escriban sus nombres:"
        Label3.Visible = True
        TextBox1.Visible = True
        PictureBox1.Visible = True
        Label4.Visible = True
        TextBox2.Visible = True
        PictureBox2.Visible = True
        ButtonJ1.Visible = False
        ButtonJ2.Visible = False
    End Sub

    Private Sub Picture1_Click(sender As Object, e As EventArgs) Handles Picture1.Click
        If t1 = 1 And t2 = 1 Then
            If Not TextBox1.Text = TextBox2.Text Then
                If My.Computer.FileSystem.FileExists("sonido.txt") Then
                    s.Play()
                End If
                If Home.LabelNum.Text = 1 Then
                    TextBox2.Text = "CPU"
                End If
                Picture1.BackColor = Color.LightBlue
                LabelJ2.Visible = True
                Picture1.Visible = True
                Picture2.Visible = False
                Picture3.Visible = False
                Picture4.Visible = True
                Picture1.Enabled = False
                Picture2.Enabled = False
                Home.J1.Text = "o"
                Home.J2.Text = "x"
                Home.LabelName1.Text = TextBox1.Text
                Home.LabelName2.Text = TextBox2.Text
                Timer.Enabled = True
            Else
                MsgBox("Nombres identicos", MsgBoxStyle.Exclamation, "Tic tac toe")
            End If
        Else
            MsgBox("Nombre(s) incorrectos(s)", MsgBoxStyle.Exclamation, "Tic tac toe")
        End If
    End Sub

    Private Sub Picture1_MouseEnter(sender As Object, e As EventArgs) Handles Picture1.MouseEnter
        Picture1.BackColor = Color.LightBlue
    End Sub

    Private Sub Picture1_MouseLeave(sender As Object, e As EventArgs) Handles Picture1.MouseLeave
        Picture1.BackColor = Color.White
    End Sub

    Private Sub Picture2_Click(sender As Object, e As EventArgs) Handles Picture2.Click
        If t1 = 1 And t2 = 1 Then
            If Not TextBox1.Text = TextBox2.Text Then
                If My.Computer.FileSystem.FileExists("sonido.txt") Then
                    s.Play()
                End If
                If Home.LabelNum.Text = 1 Then
                    TextBox2.Text = "CPU"
                End If
                Picture2.BackColor = Color.LightBlue
                LabelJ2.Visible = True
                Picture1.Visible = False
                Picture2.Visible = True
                Picture3.Visible = True
                Picture4.Visible = False
                Picture1.Enabled = False
                Picture2.Enabled = False
                Home.J1.Text = "x"
                Home.J2.Text = "o"
                Home.LabelName1.Text = TextBox1.Text
                Home.LabelName2.Text = TextBox2.Text
                Timer.Enabled = True
            Else
                MsgBox("Nombres identicos", MsgBoxStyle.Exclamation, "Tic tac toe")
            End If
        Else
            MsgBox("Nombre(s) incorrectos(s)", MsgBoxStyle.Exclamation, "Tic tac toe")
        End If
    End Sub

    Private Sub Picture2_MouseEnter(sender As Object, e As EventArgs) Handles Picture2.MouseEnter
        Picture2.BackColor = Color.LightBlue
    End Sub

    Private Sub Picture2_MouseLeave(sender As Object, e As EventArgs) Handles Picture2.MouseLeave
        Picture2.BackColor = Color.White
    End Sub

    Private Sub Timer_Tick(sender As Object, e As EventArgs) Handles Timer.Tick
        ProgressBar1.Increment(2)
        If ProgressBar1.Value = 100 Then
            Timer.Enabled = False
            Home.Show()
            Me.Close()
        End If
    End Sub

    Dim p As String
    Dim s As System.Media.SoundPlayer

    Private Sub Menu_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        p = My.Application.Info.DirectoryPath
        s = New System.Media.SoundPlayer(p & "\menu.wav")
        t1 = 0
        t2 = 0
    End Sub

    Private Sub TextBox1_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox1.KeyPress
        Dim n1 As String
        n1 = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890.,-_@#"
        If InStr(n1, e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub

    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles TextBox1.TextChanged
        If Len(TextBox1.Text) = 0 Then
            PictureBox1.Image = My.Resources.pwbad
            Picture1.Visible = False
            Picture2.Visible = False
            t1 = 0
        End If
        If Len(TextBox1.Text) >= 1 And Len(TextBox1.Text) <= 10 Then
            PictureBox1.Image = My.Resources.pwgood
            LabelJ1.Text = TextBox1.Text & ":"
            Picture1.Visible = True
            Picture2.Visible = True
            t1 = 1
        End If
    End Sub

    Private Sub TextBox2_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox2.KeyPress
        Dim n2 As String
        n2 = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890.,-_@#"
        If InStr(n2, e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub

    Private Sub TextBox2_TextChanged(sender As Object, e As EventArgs) Handles TextBox2.TextChanged
        If Len(TextBox2.Text) = 0 Then
            PictureBox2.Image = My.Resources.pwbad
            Picture3.Visible = False
            Picture4.Visible = False
            t2 = 0
        End If
        If Len(TextBox2.Text) >= 1 And Len(TextBox2.Text) <= 10 Then
            PictureBox2.Image = My.Resources.pwgood
            LabelJ2.Text = TextBox2.Text & ":"
            Picture3.Visible = True
            Picture4.Visible = True
            t2 = 1
        End If
    End Sub

    Private Sub Picture4_Click(sender As Object, e As EventArgs) Handles Picture4.Click
        If t1 = 1 And t2 = 1 Then
            If Not TextBox1.Text = TextBox2.Text Then
                If My.Computer.FileSystem.FileExists("sonido.txt") Then
                    s.Play()
                End If
                Picture1.BackColor = Color.LightBlue
                LabelJ2.Visible = True
                Picture1.Visible = True
                Picture2.Visible = False
                Picture3.Visible = False
                Picture4.Visible = True
                Picture1.Enabled = False
                Picture2.Enabled = False
                Home.J1.Text = "o"
                Home.J2.Text = "x"
                Home.LabelName1.Text = TextBox1.Text
                Home.LabelName2.Text = TextBox2.Text
                Timer.Enabled = True
            Else
                MsgBox("Nombres identicos", MsgBoxStyle.Exclamation, "Tic tac toe")
            End If
        Else
            MsgBox("Nombre(s) incorrectos(s)", MsgBoxStyle.Exclamation, "Tic tac toe")
        End If
    End Sub

    Private Sub Picture3_Click(sender As Object, e As EventArgs) Handles Picture3.Click
        If t1 = 1 And t2 = 1 Then
            If Not TextBox1.Text = TextBox2.Text Then
                If My.Computer.FileSystem.FileExists("sonido.txt") Then
                    s.Play()
                End If
                Picture2.BackColor = Color.LightBlue
                LabelJ2.Visible = True
                Picture1.Visible = False
                Picture2.Visible = True
                Picture3.Visible = True
                Picture4.Visible = False
                Picture1.Enabled = False
                Picture2.Enabled = False
                Home.J1.Text = "x"
                Home.J2.Text = "o"
                Home.LabelName1.Text = TextBox1.Text
                Home.LabelName2.Text = TextBox2.Text
                Timer.Enabled = True
            Else
                MsgBox("Nombres identicos", MsgBoxStyle.Exclamation, "Tic tac toe")
            End If
        Else
            MsgBox("Nombre(s) incorrectos(s)", MsgBoxStyle.Exclamation, "Tic tac toe")
        End If
    End Sub
End Class