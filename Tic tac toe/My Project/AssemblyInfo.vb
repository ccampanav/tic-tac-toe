﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' La información general sobre un ensamblado se controla mediante el siguiente 
' conjunto de atributos. Cambie estos atributos para modificar la información
' asociada con un ensamblado.

' Revisar los valores de los atributos del ensamblado

<Assembly: AssemblyTitle("Tic tac toe")> 
<Assembly: AssemblyDescription("Juego de Tic tac toe para 1 o 2 jugadores")> 
<Assembly: AssemblyCompany("CCV Security")> 
<Assembly: AssemblyProduct("Tic tac toe")> 
<Assembly: AssemblyCopyright("CCV Security. © Reservados todos los derechos.  2015")> 
<Assembly: AssemblyTrademark("Tic tac toe")> 

<Assembly: ComVisible(False)>

'El siguiente GUID sirve como identificador de typelib si este proyecto se expone a COM
<Assembly: Guid("3c3f1224-7be1-4ec4-9dd5-a0d7f73cff7e")> 

' La información de versión de un ensamblado consta de los cuatro valores siguientes:
'
'      Versión principal
'      Versión secundaria 
'      Número de compilación
'      Revisión
'
' Puede especificar todos los valores o usar los valores predeterminados de número de compilación y de revisión 
' mediante el asterisco ('*'), como se muestra a continuación:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.0.0")> 
<Assembly: AssemblyFileVersion("1.0.0.0")> 
